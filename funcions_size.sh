#!/bin/bash
#Marcel Torres @EDT 2019-2020
#isx26318456
#Practica 3 m01 
#Funcions size 

#1.-fsize Donat un login calcular amb du l'ocupació del home de l'usuari. Cal obtenir el home del /etc/passwd.

function fsize(){
  user=$1
  home=$(grep "^$user:" /etc/passwd | cut -d: -f6) 
  size=$(du -s $home 2> /dev/null)
  echo $size 
  return 0
}

#2.-loginargs Aquesta funció rep logins i per a cada login es mostra l'ocupació de disc del home de l'usuari usant fsize. Verificar que es rep almenys un argument. Per a cada argument verificar si és un login vàlid, si no generra una traça d'error.

function loginargs(){
ERROR_ARGS=1
ERROR_LOGIN=2
loginlist=$*
if [ $# -lt 1 ]; then
  echo "ERROR: Nº args incorrecte" >> /dev/stderr 
  echo "USAGE: function login[...]" >> /dev/stderr
  return $ERROR_ARGS
fi 
for user in $loginlist
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: unexistent user" >> /dev/stderr
    return $ERROR_LOGIN
  else 
    fsize $user
  fi
done
return 0
} 

#3.-loginfile Rep com a argument un nom de fitxer que conté un lògin per línia. Mostrar l'ocupació de disc de cada usuari usant fsize. Verificar que es rep un argument i que és un regular file.

function loginfile(){
ERROR_ARGS=1
ERROR_FILE=2
ERROR_LOGIN=3
file=$1

if [ $# -ne 1 ]; then 
  echo "ERROR: Nº args incorrecte" >> /dev/stderr
  echo "USAGE: loginfile file" >> /dev/stderr
  return $ERROR_ARGS
fi

if ! [ -f $1 ]; then 
  echo "ERROR: $1 is not a regular file" >> /dev/stderr
  return $ERROR_FILE
fi

while read -r user
do 
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: unexistent user" >> /dev/stderr
    return $ERROR_LOGIN
  else
    fsize $user
  fi
done < $file
return 0 
} 
#4.-loginboth Rep com a argument un file o res (en aquest cas es processa stdin). El fitxer o stdin contenen un lògin per línia. Mostrar l'ocupació de disc del home de l'usuari. Verificar els arguments rebuts. verificar per cada login rebut que és vàlid.

function loginboth(){
ERROR_FILE=1
ERROR_LOGIN=2
file=$1 

if [ $# -ne 1 ]; then
  echo "use stdin"
  echo "usage with stdin: loginboth"
  file=/dev/stdin
fi

if ! [ -f $1 ]; then
  echo "ERROR: $1 is not a regular file" >> /dev/stderr
  return $ERROR_FILE
fi

while read -r user
do
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -ne 0 ]; then
    echo "ERROR: unexistent user" >> /dev/stderr
    return $ERROR_LOGIN
  else
    fsize $user
  fi
done < $file
return 0
} 

#5.-grepgid Donat un GID com a argument, llistar els logins dels usuaris que petanyen a aquest grup com a grup principal. Verificar que es rep un argument i que és un GID vàlid.

function grepgid(){
ERROR_ARGS=1
ERROR_GID=2
gid=$1

if [ $# -ne 1 ]; then
  echo "ERROR: Nº args incorrecte" >> /dev/stderr
  echo "USAGE: grepgid gid" >> /dev/stderr
  return $ERROR_ARGS
fi

grep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then 
  echo "ERROR: $gid is an unexistent group" >> /dev/stderr
  return $ERROR_GID
fi

grep "^[^:]*:[^:]*:[^:]*:$gid:" /etc/passwd | cut -d: -f1
return 0
}
#6.-gidsize Donat un GID com a argument, mostrar per a cada usuari que pertany a aquest grup l'ocupació de disc del seu home. Verificar que es rep un argument i que és un gID vàlid.

function gidsize(){
ERROR_ARGS=1
ERROR_GID=2
gid=$1
Userlist=$(grepgid $gid)

if [ $# -ne 1 ]; then
  echo "ERROR: Nº args incorrecte" >> /dev/stderr
  echo "USAGE: gidsize gid" >> /dev/stderr
  return $ERROR_ARGS
fi

grep "^[^:]*:[^:]*:$gid:" /etc/group &> /dev/null
if [ $? -ne 0 ]; then
  echo "ERROR: $gid is an unexistent group" >> /dev/stderr
  return $ERROR_GID
fi
 
for user in $Userlist  
do
  fsize $user
done
return 0
} 
#7.-allgidsize Llistar de tots els GID del sistema (en ordre creixent) l'ocupació del home dels usuaris que hi pertanyen.

function allgidsize(){
gid=$(cut -d: -f3 /etc/group)
Gidlist=$(cut -d: -f3 /etc/group) 
for gid in $Gidlist
do
  gidsize $gid
done
} 

#8.-allgroupsize Llistar totes les línies de /etc/group i per cada llínia llistar l'ocupació del home dels usuaris que hi pertanyen. Ampliar filtrant només els grups del 0 al 100.

function allgroupsize(){
while read -r line
do
  gid=$(echo $line | cut -d: -f3)
  if [ $gid -ge 0 -a $gid -le 100 ]; then
    gidsize $gid
  fi
done < /etc/group
return 0
} 

#9.-fstype Donat un fstype llista el device i el mountpoint (per odre de device) de les entrades de fstab d'quest fstype.
function fstype(){  
  fstype=$1  
  fstab=$(egrep -v "^#|^$" /etc/fstab | tr -s '[:blank:]' ':')  
  echo "$fstab" | grep "^[^:]*:[^:]*:$fstype:" | cut -d: -f1,2 | sort | tr ':' '\t'  
  return 0
}

#10.-allfstype LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint.
function allfstype(){
fstab=$(egrep -v "^#|^$" /etc/fstab | tr -s '[:blank:]' ':')   
fstypelist=$(echo "$fstab" | cut -d: -f3 | sort -u)   
for type in $fstypelist
do     
  fstype $type  
done   
return 0

#11.-allfstypeif LLista per a cada fstype que existeix al fstab (per ordre lexicogràfic) les entrades d'quest tipus. Llistar tabuladament el device i el mountpoint. Es rep un valor numèric d'argument que indica el numéro mínim d'entrades d'aquest fstype que hi ha d'haver per sortir al llistat.
function allfstypeif(){
minim=$1   
fstab=$(egrep -v "^#|^$" /etc/fstab | tr -s '[:blank:]' ':')  
linesfstab=$(echo "$fstab" | wc -l)   
if [ "$linesfstab" -lt "$minim" ]; then     
  echo "ERROR: less than $minim" >> /dev/stderr     
  return 1  
fi
allfstype
return 0
} 
